/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
______     ______     ______   __  __     __     ______
/\  == \   /\  __ \   /\__  _\ /\ \/ /    /\ \   /\__  _\
\ \  __<   \ \ \/\ \  \/_/\ \/ \ \  _"-.  \ \ \  \/_/\ \/
\ \_____\  \ \_____\    \ \_\  \ \_\ \_\  \ \_\    \ \_\
\/_____/   \/_____/     \/_/   \/_/\/_/   \/_/     \/_/


This is a sample Cisco Spark bot built with Botkit.

# RUN THE BOT:
Follow the instructions here to set up your Cisco Spark bot:
-> https://developer.ciscospark.com/bots.html
Run your bot from the command line:
access_token=<MY BOT ACCESS TOKEN> public_address=<MY PUBLIC HTTPS URL> node bot.js



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
require('./env.js')
const wit = require('./wit')
const path = require('path')
const fs = require('fs')
const messages = require('./messagesController')
const WitRouter = require('wit-router')
const witRouter = new WitRouter()

if (!process.env.access_token) {
  console.log('Error: Specify a Cisco Spark access_token in environment.');
  usage_tip();
  process.exit(1);
}

if (!process.env.public_address) {
  console.log('Error: Specify an SSL-enabled URL as this bot\'s public_address in environment.');
  usage_tip();
  process.exit(1);
}

var Botkit = require('botkit');
var debug = require('debug')('botkit:main');

// Create the Botkit controller, which controls all instances of the bot.
var controller = Botkit.sparkbot({
  // debug: true,
  // limit_to_domain: ['mycompany.com'],
  // limit_to_org: 'my_cisco_org_id',
  public_address: process.env.public_address,
  ciscospark_access_token: process.env.access_token,
  studio_token: process.env.studio_token, // get one from studio.botkit.ai to enable content management, stats, message console and more
  secret: process.env.secret, // this is an RECOMMENDED but optional setting that enables validation of incoming webhooks
  webhook_name: 'Cisco Spark bot created with Botkit, override me before going to production',
  studio_command_uri: process.env.studio_command_uri,
});

// Set up an Express-powered webserver to expose oauth and webhook endpoints
var webserver = require(__dirname + '/components/express_webserver.js')(controller);

// Tell Cisco Spark to start sending events to this application
require(__dirname + '/components/subscribe_events.js')(controller);

// Enable Dashbot.io plugin
require(__dirname + '/components/plugin_dashbot.js')(controller);

// get recursive list of files
var walk = function(dir, done) {
  var results = []
  fs.readdir(dir, function(err, list) {
    if (err) return done(err)
    var pending = list.length
    if (!pending) return done(null, results)
    list.forEach(function(file) {
      file = path.resolve(dir, file)
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            results = results.concat(res)
            if (!--pending) done(null, results)
          })
        } else {
          results.push(file)
          if (!--pending) done(null, results)
        }
      })
    })
  })
}



// require all messages recursively
walk(path.join(__dirname, 'messages'), function(err, results) {
  if (err) throw err
  console.log(results)
  results.forEach(file => {
    require(file)(messages)
  })
})

// require all skills recursively, and pass the message controller
walk(path.join(__dirname, 'skills'), function(err, results) {
  if (err) throw err
  console.log(results)
  results.forEach(file => {
    require(file)(witRouter, messages)
  })
})

// This captures and evaluates any message sent to the bot as a DM
// or sent to the bot in the form "@bot message" and passes it to
// wit.ai for NLP.
controller.hears(['.*'], 'direct_mention,direct_message', async (bot, message) => {
  let entities
  try {
    const response = await wit(message.text)
    entities = response.data.entities
    console.log('wit.ai entities = ', entities)
  } catch (e) {
    // wit.ai REST error
    console.log(e)
    bot.startConversation(message, (err, convo) => {
      convo.addMessage(messages.get('core.no-nlp'), 'default')
      convo.activate()
    })
  }

  let route
  try {
    // attempt to route using entities
    route = witRouter.route(entities)
  } catch (e) {
    // no route
    console.log(e)
    bot.startConversation(message, (err, convo) => {
      convo.addMessage(messages.get('core.fallback'), 'default')
      convo.activate()
    })
    return
  }

  try {
    // run callback on best matching route
    route.callback(bot, message, entities)
    return
  } catch (e) {
    // error during conversation
    console.log(e)
    bot.startConversation(message, (err, convo) => {
      convo.addMessage(messages.get('core.convo-error'), 'default')
      convo.activate()
    })
    return
  }
})
