const axios = require('axios')
require('./env.js')

module.exports = async (query) => {
  let options = {
    headers: {
      'Authorization': 'Bearer ' + process.env.wit_token
    },
    params: {
      v: process.env.wit_version,
      q: query
    }
  }
  let url = 'https://api.wit.ai/message'
  return await axios.get(url, options)
}
