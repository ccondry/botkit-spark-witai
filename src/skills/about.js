const matches = {
  intent: [{
    match: /^about$/i,
    confidence: 0.7
  }]
}

module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'about',
    entities: matches,
    callback: function (bot, message, entities) {
      bot.startConversation(message, (err, convo) => {
        convo.addMessage(messages.get('about'), 'default')
        convo.activate()
      })
    }
  })
}
