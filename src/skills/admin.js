const matches = {
  intent: [{
    match: /^admin$/i,
    confidence: 0.7
  }]
}

module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'admin',
    entities: matches,
    callback: function (bot, message, entities) {
      bot.startConversation(message, (err, convo) => {
        convo.addMessage(messages.get('admin.test'), 'default')
        convo.activate()
      })
    }
  })
}
