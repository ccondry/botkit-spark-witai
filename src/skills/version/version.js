/**
Meta Version
**/
const matches = {
  intent: [{
    match: /^version$/
  }]
}

module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'version',
    entities: matches,
    callback: function (bot, message, entities) {
      // start a conversation
      bot.startConversation(message, (err, convo) => {
        convo.setVar('version', process.env.npm_package_version)
        convo.setVar('mode', process.env.NODE_ENV)
        // no server entities
        // prepare default thread
        convo.addMessage(messages.get('meta.version.done'), 'default')
        // go
        convo.activate()
      })
    }
  })
}
