/**
CVP Version
**/
const matches = {
  intent: [{
    match: /^version$/
  }],
  server: [{
    match: /^CVP$/
  }]
}

module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'version',
    entities: matches,
    callback: function (bot, message, entities) {
      bot.startConversation(message, (err, convo) => {
        // CVP server entity but no service entities
        // prepare default thread
        convo.addMessage(messages.get('cvp.version.no-service'), 'default')
        // go
        convo.activate()
      })
    }
  })
}
