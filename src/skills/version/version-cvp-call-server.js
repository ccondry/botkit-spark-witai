/**
CVP Call Server Version
**/
const matches = {
  intent: [{
    match: /^version$/
  }],
  server: [{
    match: /^CVP$/
  }],
  service: [{
    match: /^Call Server$/,
    required: true
  }]
}

// get CVP Call Server version
module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'version-cvp-call',
    entities: matches,
    callback: function (bot, message, entities) {
      bot.startConversation(message, (err, convo) => {
        // initial acknowledgement
        convo.addMessage({
          text: messages.get('cvp.version.starting'),
          action: 'completed' // next thread
        }, 'default')
        // prepare completed thread
        convo.addMessage(messages.get('cvp.version.done'), 'completed')
        // prepare error thread
        convo.addMessage(messages.get('cvp.version.error'), 'error')

        // set the service variable
        convo.setVar('service', 'Call Server')

        // get data and prepare answer before delivering it
        convo.beforeThread('completed', async function (convo, next) {
          // make the API request
          try {
            console.log('getting CVP Call Server version for user ', convo.context.user)
            // normally you would make a synchronous request here to get the version number
            convo.setVar('version', '11.5')
            // call next to continue to the conversation
            next()
          } catch(error) {
            // API request failed
            console.log('error', error)
            convo.setVar('error', error)
            convo.gotoThread('error')
            // call next and pass an error because we changed threads again during this transition
            next(error)
          }
        })
        // go
        convo.activate()
      })
    }
  })
}
