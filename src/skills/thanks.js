module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'thanks',
    entities: {
      intent: [{
        match: /^thank$/i,
        confidence: 0.9
      }]
    },
    callback: function (bot, message, entities) {
      bot.startConversation(message, (err, convo) => {
        convo.addMessage(messages.get('youre-welcome'), 'default')
        convo.activate()
      })
    }
  })
}
