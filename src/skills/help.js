const matches = {
  intent: [{
    match: /^help$/i,
    confidence: 0.7
  }]
}

const skills = [
  'CVP Call Server version',
  'about',
  'help',
  'version'
]

const adminSkills = [
  'admin'
]

module.exports = function (witRouter, messages) {
  witRouter.register({
    name: 'help',
    entities: matches,
    callback: function (bot, message, entities) {
      // start a conversation
      bot.startConversation(message, (err, convo) => {
        // set up skill variables
        convo.setVar('skills', skills)
        // list skills
        convo.addMessage({
          text: messages.get('skills2'),
          action: 'admin'
        }, 'default')
        // set up admin skills thread
        convo.addMessage(messages.get('skills.admin'), 'admin')
        // check if user is admin before showing them the admin skills
        convo.beforeThread('admin', async function (convo, next) {
          try {
            console.log('looking up user info to check if user is admin')
            // load user info
            // const user = await account.getUser(convo.context.user)
            const user = {admin: true}
            console.log('received user', user)
            // TODO cache results of user info for this bot
            // check if user is an admin
            if (!user.admin) {
              console.log('user is admin. show them more skills!')
              // display admin skills for user
              convo.setVar('adminSkills', adminSkills)
              next()
            } else {
              console.log('user is not an admin. stop conversation.')
              // don't show the admin skills
              convo.stop()
            }
          } catch (e) {
            console.log('error getting user account info:', e)
            // error getting user account info
            // end conversation
            convo.stop()
          }
        })
        convo.activate()
      })
    }
  })
}
