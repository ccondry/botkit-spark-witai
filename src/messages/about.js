module.exports = function (mc) {
  mc.add('about', `Hello, I'm the CXDemo Toolbot written by Coty Condry <ccondry@cisco.com>. I run on JavaScript using the howdy.ai/botkit framework for conversation management, and I have natural language processing capabilities from wit.ai. I run in the CXDemo Lab in dCloud, and I will try to help you with lab tasks.`)
}
