module.exports = function (mc) {
  mc.add('core.no-nlp', `Hey, I'm sorry, but I'm not able to contact the NLP web service right now. Ask me again later.`)
  mc.add('core.convo-error', `Hey, I'm sorry, but I'm having trouble coming up with the right things to say. Ask me again later?`)
  mc.add('core.fallback', `Sorry, I don't think I can help you with that. Say 'help' if you want me to talk about what I can do.`)
  mc.add('core.too-many', `I don't think I can do all of that at once. Could you ask me for one thing at a time?`)
  mc.add('core.no-match', `Sorry, I didn't get that.`)
}
