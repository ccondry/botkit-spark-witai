module.exports = function (mc) {
  mc.add('skills', `Here are some things that I can help you with: \n\n{{#vars.skills}}* {{skill}}\n\n{{/vars.skills}}`)
  mc.add('skills2', `Here are some things that I can help you with: \n\n{{#vars.skills}}* {{.}}\n\n{{/vars.skills}}`)
  mc.add('skills.servers', `Here's what I can do: \n\n{{#vars.skills}}* {{skill}}\n\n{{/vars.skills}}`)
  mc.add('skills.servers.unknown', `Sorry, I don't know much about that server yet.`)
  mc.add('skills.admin', `I can also help with these admin tasks: \n\n{{#vars.adminSkills}}* {{.}}\n\n{{/vars.adminSkills}}`)
}
