module.exports = function (mc) {
  mc.add('meta.version.done', `I'm the CXDemo Toolbot version {{vars.version}} running in {{vars.mode}} mode.`)
  mc.add('cvp.version.starting', `I'm retrieving the CVP {{vars.service}} version now...`)
  mc.add('cvp.version.done', `The CVP {{vars.service}} is {{vars.version}}`)
  mc.add('cvp.version.error', `I'm sorry, I failed to get the CVP {{vars.service}} version info. The message I did get back was '{{vars.error}}'.`)
  mc.add('cvp.version.no-service', `I can get you the CVP Call Server's version, if you like. Just say something like 'What is the CVP Call Server version?'`)
}
