const messages = {}

module.exports = {
  add: function (key, message, language = 'en-us') {
    // add language key if not exists
    if (!messages[language]) messages[language] = {}
    // set message
    messages[language][key] = message
  },
  get: function (key, language = 'en-us') {
    return messages[language][key]
  }
}
